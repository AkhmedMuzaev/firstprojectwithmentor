// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBFL.h"

FVector UMyBFL:: MyVector = FVector(1.0f,1.0f,1.0f);

int32 UMyBFL::MyNumber(int32 a, int32 b)
{
	return a+b;
}

FString UMyBFL::MyString()
{
	return FString(TEXT("Hello Unreal"));
}

void UMyBFL::VectorFunction(float X, float Y, float Z)
{
	MyVector = FVector(X,Y,Z);
}

void UMyBFL::WorldOfSet(AActor* Actor)
{
	if (IsValid(Actor))
	{
		Actor->AddActorWorldOffset(FVector(6.0f,4.0f,3.0f));
	}
}

FVector UMyBFL::PrintVector()
{
	return MyVector;
}


