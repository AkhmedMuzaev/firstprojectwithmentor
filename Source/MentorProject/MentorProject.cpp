// Copyright Epic Games, Inc. All Rights Reserved.

#include "MentorProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MentorProject, "MentorProject" );
