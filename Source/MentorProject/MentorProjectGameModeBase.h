// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MentorProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MENTORPROJECT_API AMentorProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
