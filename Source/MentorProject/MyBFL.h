// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBFL.generated.h"

/**
 * 
 */
UCLASS()
class MENTORPROJECT_API UMyBFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	static FVector MyVector;

	UFUNCTION(BlueprintCallable, Category="MyFunctions")
	static int32 MyNumber(int32 a, int32 b);

	UFUNCTION(BlueprintCallable, Category="MyFunctions")
	static  FString MyString();

	UFUNCTION(BlueprintCallable, Category="MyFunctions")
	static void VectorFunction(float X, float Y, float Z);

	UFUNCTION(BlueprintCallable, Category="MyFunctions")
	static FVector PrintVector();

	UFUNCTION(BlueprintCallable, Category="MyFunctions")
	static void WorldOfSet(AActor* Actor);
};
