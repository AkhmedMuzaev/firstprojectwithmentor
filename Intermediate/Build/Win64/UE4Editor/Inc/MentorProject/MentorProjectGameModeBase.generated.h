// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MENTORPROJECT_MentorProjectGameModeBase_generated_h
#error "MentorProjectGameModeBase.generated.h already included, missing '#pragma once' in MentorProjectGameModeBase.h"
#endif
#define MENTORPROJECT_MentorProjectGameModeBase_generated_h

#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_SPARSE_DATA
#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_RPC_WRAPPERS
#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMentorProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMentorProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMentorProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MentorProject"), NO_API) \
	DECLARE_SERIALIZER(AMentorProjectGameModeBase)


#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMentorProjectGameModeBase(); \
	friend struct Z_Construct_UClass_AMentorProjectGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMentorProjectGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MentorProject"), NO_API) \
	DECLARE_SERIALIZER(AMentorProjectGameModeBase)


#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMentorProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMentorProjectGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMentorProjectGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMentorProjectGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMentorProjectGameModeBase(AMentorProjectGameModeBase&&); \
	NO_API AMentorProjectGameModeBase(const AMentorProjectGameModeBase&); \
public:


#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMentorProjectGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMentorProjectGameModeBase(AMentorProjectGameModeBase&&); \
	NO_API AMentorProjectGameModeBase(const AMentorProjectGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMentorProjectGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMentorProjectGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMentorProjectGameModeBase)


#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_12_PROLOG
#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_SPARSE_DATA \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_RPC_WRAPPERS \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_INCLASS \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_SPARSE_DATA \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	MentorProject_Source_MentorProject_MentorProjectGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MENTORPROJECT_API UClass* StaticClass<class AMentorProjectGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MentorProject_Source_MentorProject_MentorProjectGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
