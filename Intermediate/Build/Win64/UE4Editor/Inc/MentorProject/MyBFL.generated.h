// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
struct FVector;
#ifdef MENTORPROJECT_MyBFL_generated_h
#error "MyBFL.generated.h already included, missing '#pragma once' in MyBFL.h"
#endif
#define MENTORPROJECT_MyBFL_generated_h

#define MentorProject_Source_MentorProject_MyBFL_h_15_SPARSE_DATA
#define MentorProject_Source_MentorProject_MyBFL_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execWorldOfSet); \
	DECLARE_FUNCTION(execPrintVector); \
	DECLARE_FUNCTION(execVectorFunction); \
	DECLARE_FUNCTION(execMyString); \
	DECLARE_FUNCTION(execMyNumber);


#define MentorProject_Source_MentorProject_MyBFL_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWorldOfSet); \
	DECLARE_FUNCTION(execPrintVector); \
	DECLARE_FUNCTION(execVectorFunction); \
	DECLARE_FUNCTION(execMyString); \
	DECLARE_FUNCTION(execMyNumber);


#define MentorProject_Source_MentorProject_MyBFL_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyBFL(); \
	friend struct Z_Construct_UClass_UMyBFL_Statics; \
public: \
	DECLARE_CLASS(UMyBFL, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MentorProject"), NO_API) \
	DECLARE_SERIALIZER(UMyBFL)


#define MentorProject_Source_MentorProject_MyBFL_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyBFL(); \
	friend struct Z_Construct_UClass_UMyBFL_Statics; \
public: \
	DECLARE_CLASS(UMyBFL, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MentorProject"), NO_API) \
	DECLARE_SERIALIZER(UMyBFL)


#define MentorProject_Source_MentorProject_MyBFL_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyBFL(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBFL) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBFL); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBFL); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBFL(UMyBFL&&); \
	NO_API UMyBFL(const UMyBFL&); \
public:


#define MentorProject_Source_MentorProject_MyBFL_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyBFL(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyBFL(UMyBFL&&); \
	NO_API UMyBFL(const UMyBFL&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyBFL); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyBFL); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyBFL)


#define MentorProject_Source_MentorProject_MyBFL_h_15_PRIVATE_PROPERTY_OFFSET
#define MentorProject_Source_MentorProject_MyBFL_h_12_PROLOG
#define MentorProject_Source_MentorProject_MyBFL_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MentorProject_Source_MentorProject_MyBFL_h_15_PRIVATE_PROPERTY_OFFSET \
	MentorProject_Source_MentorProject_MyBFL_h_15_SPARSE_DATA \
	MentorProject_Source_MentorProject_MyBFL_h_15_RPC_WRAPPERS \
	MentorProject_Source_MentorProject_MyBFL_h_15_INCLASS \
	MentorProject_Source_MentorProject_MyBFL_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MentorProject_Source_MentorProject_MyBFL_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MentorProject_Source_MentorProject_MyBFL_h_15_PRIVATE_PROPERTY_OFFSET \
	MentorProject_Source_MentorProject_MyBFL_h_15_SPARSE_DATA \
	MentorProject_Source_MentorProject_MyBFL_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MentorProject_Source_MentorProject_MyBFL_h_15_INCLASS_NO_PURE_DECLS \
	MentorProject_Source_MentorProject_MyBFL_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MENTORPROJECT_API UClass* StaticClass<class UMyBFL>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MentorProject_Source_MentorProject_MyBFL_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
